
        // this is js file for search 
        
function getCookie(name) {
	var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
	return r ? r[1] : undefined;
}


jQuery.postJSON = function(url, args, callback) {
  args._xsrf = getCookie("_xsrf");
	$.ajax({
			url: url,
			data: $.param(args),
			dataType: "text",
			type: "POST",
			success: function(response) {
				callback( response );
			}
	});
};


var user_info  = function(){
	$(".online").html('<input type="hidden" id="online-0" >');
	$.postJSON(
		server_ip+"/search",
		{"obj":'{"type":"search"}'},
		function(data){ 
			obj = JSON.parse(data);
			for (var i =0 ; i < obj.res.length ; i++){
				$("#online-0").after('<span class="label label-danger" style="margin:10px">'+obj.res[i]+'</span>');
			}
		});
}
