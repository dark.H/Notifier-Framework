

## write by qingluan 
# this is a config file
# include db and debug , static path 

import motor 
from os import path
# here to load all controllers

from controller import *
from sys import argv

# db engine 
db_engine = motor.MotorClient()

# static path 
static_path = "./static"


Settings = {
        'db':db_engine,
        'debug':True,
        'autoreload':True,
        'cookie_secret':'This string can be any thing you want',
        'static_path' : static_path,
        'self_ip':"blog.qingluan.info:9999",
    }


## follow is router
test_ip = '192.168.1.100:9999'
if len(argv) > 2 and argv[2] == "test" :
    Settings['self_ip'] = test_ip
appication = tornado.web.Application([
                (r'/',IndexHandler),
                # add some new route to router
		(r'/noty',NotyHandler),
		(r'/sendmsg/(\w+?)_to_(\w+?)/msg_(.+)',SendMsgHandler),
		(r'/search',SearchHandler),
		(r'/info',InfoHandler),
		(r'/sendajaxmsg',SendajaxmsgHandler),
		(r'/toall/(\w+?)/(.+)',ToallHandler),
                (r'/links/([\w=]+?)/(.+)',LinksHandler),
                (r'/linkreply/(\w+?)/(.+)',LinkreplyHandler),
		(r'/linkstart/(\d+)',LinkstartHandler),
#<route></route>
                # (r'/main',MainHandler),
         ],**Settings)


# setting port 
port = 9999
if __name__ == "__main__":
    appication.listen(port)
    tornado.ioloop.IOLoop.instance().start() 

